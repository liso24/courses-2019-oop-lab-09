package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.*;

public class AnotherConcurrentGUI extends ConcurrentGUI2{
	
	private static final int WAITING_TIME = 10_000;

	public AnotherConcurrentGUI() {
		super();
		TimeAgent time = new TimeAgent();
		new Thread(time).start();
	}
	
	
	private class TimeAgent implements Runnable{

		@Override
		public void run() {
			try {
				Thread.sleep(WAITING_TIME);
				AnotherConcurrentGUI.this.agent.stopCounting();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
}
