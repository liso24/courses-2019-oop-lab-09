package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix{

	private final int nthread;
	
	public MultiThreadedSumMatrix(final int nthread) {
		this.nthread = nthread;
	}
	
	private static class Worker extends Thread {
		
		private final double[][] matrix;
		private final int size;
		private final int nelem; //elemnti da sommare
		private final int startpos;
		//private final int line;
		private double res;
		
		Worker(final double[][] matrix, final int startpos, final int nelem) {
			super();
			this.matrix = matrix;
			this.size = matrix.length;
			this.nelem = nelem;
			this.startpos = startpos;
			//this.line = line;
		}
		
		public void run() {
			System.out.println("Working in from LINE: " + this.startpos + " to " + (this.startpos + this.nelem - 1));
			for (int i = this.startpos; i < this.size && i < this.startpos + this.nelem; i++) {
				for (int j = 0; j < this.size; j++) {
					this.res = this.res + this.matrix[i][j];
				}
			}
		}
		
		public double getResult() {
			return this.res;
		}
	}
	
	@Override
	public double sum(double[][] matrix) {
		final int size = matrix.length % nthread + matrix.length / nthread;
		
		final List<Worker> workers = new ArrayList<>(nthread);
		//for (int line = 0; line < matrix.length; line += size) {
			for (int start = 0; start < matrix.length; start += size) {
				workers.add(new Worker(matrix, start, size));
			//}
		}
		
		for (final Worker w : workers) {
			w.start();
		}
		
		double sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
		
		return sum;
	}

}
