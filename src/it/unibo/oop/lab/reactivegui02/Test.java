package it.unibo.oop.lab.reactivegui02;

/**
 * Exercise on a reactive GUI.
 */
public final class Test {

    /**
     *
     * @param args
     *            possible args to pass (not used)
     *
     */
    public static void main(final String... args) {
        new ConcurrentGUI2();
    }
}
