
	
package it.unibo.oop.lab.reactivegui02;
	
import java.awt.Dimension;
	import java.awt.Toolkit;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JPanel;
	import javax.swing.SwingUtilities;
	
	/**
	 * Exercise on a reactive GUI.
	 */
public class ConcurrentGUI2 {
	
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
	
    private final JLabel label = new JLabel();
    private final JButton up = new JButton("up");
	private final JButton down = new JButton("down");
	private final JButton stop = new JButton("stop");
	protected final Agent agent;
	
	public ConcurrentGUI2() {
	
	    JFrame frame = new JFrame();
	    final Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
	    frame.setSize((int) (screensize.getWidth() * WIDTH_PERC), (int) (screensize.getHeight() * HEIGHT_PERC));
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	    JPanel panel = new JPanel();
	    panel.add(this.label);
	    panel.add(this.up);
	    panel.add(this.down);
	    panel.add(this.stop);
	
	    this.agent = new Agent();
	    new Thread(agent).start();
	
	    up.addActionListener(new ActionListener() {
	
	        @Override
	        public void actionPerformed(final ActionEvent e) {
	            agent.setUp();
	            ConcurrentGUI2.this.up.setEnabled(false);
	            ConcurrentGUI2.this.down.setEnabled(true);
	        }
	    });
	
	    down.addActionListener(new ActionListener() {
	
	        @Override
	        public void actionPerformed(final ActionEvent e) {
	            agent.setDown();
	            ConcurrentGUI2.this.down.setEnabled(false);
	            ConcurrentGUI2.this.up.setEnabled(true);
	        }
	    });
	
	    stop.addActionListener(new ActionListener() {
	
	        @Override
	        public void actionPerformed(final ActionEvent e) {
	            agent.stopCounting();
	            ConcurrentGUI2.this.stop.setEnabled(false);
	            ConcurrentGUI2.this.up.setEnabled(false);
	            ConcurrentGUI2.this.down.setEnabled(false);
	        }
	    });
	
	    frame.getContentPane().add(panel);
	    frame.setVisible(true);
	}
	
	/*
	 * Realizzare una classe ConcurrentGUI con costruttore privo di argomenti,
	 * tale che quando istanziata crei un JFrame con l'aspetto mostrato nella
	 * figura allegata (e contatore inizialmente posto a zero).
	 *
	 * Il contatore venga aggiornato incrementandolo ogni 100 millisecondi
	 * circa, e il suo nuovo valore venga mostrato ogni volta (l'interfaccia sia
	 * quindi reattiva).
	 *
	 * Alla pressione del pulsante "down", il conteggio venga da lì in poi
	 * aggiornato decrementandolo; alla pressione del pulsante "up", il
	 * conteggio venga da lì in poi aggiornato incrementandolo; e così via, in
	 * modo alternato.
	 *
	 * Alla pressione del pulsante "stop", il conteggio si blocchi, e i tre
	 * pulsanti vengano disabilitati. Per far partire l'applicazioni si tolga il
	 * commento nel main qui sotto.
	 *
	 * Suggerimenti: - si mantenga la struttura dell'esercizio precedente - per
	 * pilotare la direzione su/giù si aggiunga un flag booleano all'agente --
	 * deve essere volatile? - la disabilitazione dei pulsanti sia realizzata
	 * col metodo setEnabled
	 */
	
	public class Agent implements Runnable {
	
	    private volatile boolean stop;
	    private volatile int counter;
	    private volatile boolean upDown = true; //TRUE UP
	
        @Override
        public void run() {
            while (!this.stop) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {

                        @Override
                        public void run() {
                            ConcurrentGUI2.this.label.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if (this.upDown) {
                        this.counter = this.counter + 1;
                    } else {
                        this.counter = this.counter - 1;
                    }
                    Thread.sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }

        }

        public void stopCounting() {
            this.stop = true;
        }

        public void setUp() {
            this.upDown = true;
        }

        public void setDown() {
            this.upDown = false;
        }

    }
	
	}
